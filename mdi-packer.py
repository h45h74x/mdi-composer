import os
from pathlib import Path
import xml.etree.ElementTree as ET

### Parameters ###

# Input / Output directories

svgDir = os.path.join(os.path.dirname(__file__), "MaterialDesign-SVG", "svg")
buildDir = "build"

# Material Design Icon svg file format settings

pathTagName = "path"
pathDataAtrribute = "d"
mdiXmlNs = "{http://www.w3.org/2000/svg}"

### Parameters end ###


def getSvgPath(filePath):
    xml = ET.parse(filePath)
    element = xml.find("./{}{}".format(mdiXmlNs, pathTagName))
    if element is not None:
        svgPath = element.attrib[pathDataAtrribute]
        return svgPath
    return None


def getIcons(directory):
    iconCount = 0
    invalidCount = 0
    svgIcons = dict()

    for subDir, _, files in os.walk(directory):
        for fileName in files:
            filePath = subDir + os.sep + fileName
            if filePath.endswith(".svg"):
                svgPath = getSvgPath(filePath)
                if svgPath is not None:
                    iconCount += 1
                    svgIcons[os.path.splitext(fileName)[0]] = svgPath
                else:
                    invalidCount += 1
                    print("Error in file '{}'".format(filePath))

    print("Loaded {} icons. {} scanned files were invalid.".format(
        iconCount, invalidCount))

    return svgIcons


def buildXaml(outputDir, svgIconDict):
    outputFile = os.path.join(outputDir, "Icons.xaml")
    print("Building XAML resource file at '{}'".format(outputFile))

    root = ET.Element("ResourceDictionary")
    root.attrib["xmlns"] = "http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    root.attrib["xmlns:x"] = "http://schemas.microsoft.com/winfx/2006/xaml"

    for key in svgIconDict:
        icon = ET.SubElement(root, "PathGeometry")
        icon.attrib["Figures"] = svgIconDict[key]

        keySegments = key.split('-')
        keyValue = "Mdi"

        for segment in keySegments:
            keyValue += segment.capitalize()

        icon.attrib["x:Key"] = keyValue

    ET.ElementTree(root).write(outputFile)


def build():
    print("Building...")
    Path(buildDir).mkdir(parents=True, exist_ok=True)
    svgIconDict = getIcons(svgDir)

    buildXaml(buildDir, svgIconDict)
    # ... add other variants as needed

    print("Done!")


build()
