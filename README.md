# MDI Composer

This tool converts all icons from the "Material Design Icons" Project to application resource files.

## Running the script

**Python version ≥3.7 is strongly recommended!**

To execute the composer, simply execute the `mdi-composer.py` script in the project root.
You can find your icon file(s) in the `build` directory.

## Load new icons

Run `git submodule update --init` to update the svg icon repository and execute `mdi-composer.py` to build the new files.

## Output formats

While the tool can only take svg files as input data, **the following output files are supported**:

- XAML Resource Dictionary
